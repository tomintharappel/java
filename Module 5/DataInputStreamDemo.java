import java.io.*;    
public class DataInputStreamDemo {  
  public static void main(String[] args) throws IOException {  
 
    DataInputStream input = new DataInputStream(System.in);  
    String name = input.readLine();
    DataOutputStream output = new DataOutputStream(System.out);
    output.writeChars("you entered :"+name);
    output.flush();
  }  
}  