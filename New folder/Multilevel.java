class Box {

    double width;   
    double height;   
    double depth; 
    Box (double w,double h,double d)   {

        width = w;
        height = h;
        depth =d;
    }
    void getVolume() {

        double vol = width * height * depth;
        System.out.println("the volume of the box is " + vol);
    }
}
class WeightedBox extends Box {
    
    double weight;

    WeightedBox(double w, double h, double d, double m) {
      
        super(w, h, d);
        weight = m;
    }
    void getWeight()
    {
        System.out.println("Box Weight(in kg):" + weight);
    }
}
class ColourBox extends WeightedBox {

    String colour;
    ColourBox(double w, double h, double d, double m, String c) {

        super(w, h, d, m);
        colour = c;
    }
    void getColour()
    {
        System.out.println("Box Colour :" + colour);
    }
}
class Multilevel {

    public static void main(String [] Args) {

        ColourBox box1 = new ColourBox(5, 2, 2, 10, "RED");

        box1.getVolume();
        box1.getWeight();
        box1.getColour();

        ColourBox box2 = new ColourBox(10, 15, 5, 20,"BLUE");
        box2.getVolume();
        box2.getWeight();
        box2.getColour();
    }
}