import java.awt.*;
import java.awt.event.*;
import java.applet.*;
/**
 * <applet code="MouseListen" width=300 height=100></applet>
 */
public class MouseListen extends Applet
    implements MouseListener,MouseMotionListener {

        String msg = "";
        int mouseX = 0, mouseY = 0;  // coordinates of mouse

        public void init() {
            addMouseMotionListener(this);
            addMouseListener(this);
        }
        public void mouseClicked(MouseEvent me) {
            mouseX = 0;
            mouseY = 10;
            msg = "Mouse Clicked";
            repaint();
        }
        public void mouseEntered(MouseEvent me) {

            mouseX = 0;
            mouseY = 10;
            msg = "Mouse entered";
            repaint();
        }
        // Handle mouse exited
        public void mouseExited(MouseEvent me)
        {
            mouseX = 0;
            mouseY = 10;
            msg = "Mouse Exited";
            repaint();
        }
        public void mouseReleased(MouseEvent me) {
            mouseX = me.getX();
            mouseY = me.getY();
            msg ="Up";
            repaint();
        }
       public void mousePressed(MouseEvent me) {
           mouseX = me.getX();
           mouseY = me.getY();
           msg = "Down";
           repaint();
       }
       public void mouseDragged(MouseEvent me) {
           msg = "*";
           showStatus("Dragging mouse at " +mouseX + ", " + mouseY);
       }
       public void mouseMoved(MouseEvent me) {
           showStatus("Moving mouse at " + me.getX() + "," +me.getY());
       }
        public void paint(Graphics g) {
            g.drawString(msg,mouseX,mouseY);
        }
    }