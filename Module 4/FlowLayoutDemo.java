// Use Right-aligned flow layout. 
import java.awt.*; 
import java.awt.event.*; 
import java.applet.*; 
/*   <applet code="FlowLayoutDemo" width=240 height=200>   </applet> */ 
 
public class FlowLayoutDemo extends Applet { 
 
    String msg = "";   
    Checkbox windows, android, solaris, mac; 
   
    public void init() {     
        // set left-aligned flow layout  

        setLayout(new FlowLayout(FlowLayout.RIGHT,20,20)); 
   
      windows = new Checkbox("Windows", null, true);     
      android = new Checkbox("Android");     
      solaris = new Checkbox("Solaris");     
      mac = new Checkbox("Mac OS"); 
   
      add(windows);     
      add(android);     
      add(solaris);     
      add(mac);   
    }
}
    