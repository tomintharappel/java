import java.awt.*;
import java.applet.*;

/**
 * <applet code="ScrollingBanner" width="300" height="50" ></applet>
 */

public class ScrollingBanner extends Applet implements Runnable{

    String msg = "A Simple Moving Banner";
    Thread thread = null;
    int state;
    boolean stopFlag;

    public void init() {

        setBackground(Color.cyan);
        setForeground(Color.red);
    }

    public void start() {

        thread = new Thread(this);
        stopFlag = false;
        thread.start();
    }
    public void run() {

        char ch;
        while(true) {
            try {
                repaint();
                Thread.sleep(250);
                ch = msg.charAt(0);
                msg = msg.substring(1,msg.length());
                msg += ch;

                if( stopFlag ) break;

            } catch (InterruptedException e)
            {

            }
        }
    }
    public void stop()
    {
        stopFlag = true;
        thread = null;
    }
    public void paint(Graphics g) {

        g.drawString(msg,50,30);
    }
}