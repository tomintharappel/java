class MyException extends Exception {
    MyException(String message) {
        super(message);
    }
}
class UserException {
    public static void main(String []args) {
        float x =5, y =1000;
        try {
            float z = x/y;
            if(z< 0.01)
            {
                throw new MyException("Number is too small");
            }
        }
        catch(MyException e) {
            System.out.println("Caught my exception");
            System.out.println(e.getMessage());
        }
        
    }
}