import java.awt.*;  
import java.awt.event.*;  
import java.applet.*; 
/*<applet code="Calculator.java" width="300" height="300"></applet> */
public class Calculator extends Applet implements ActionListener  
{  
    TextField t1 = new TextField(10);  
    TextField t2 = new TextField(10);  
    Label t3 = new Label("N/A");  
    Label l1 = new Label("FIRST NO:");  
    Label l2 = new Label("SECOND NO:");  
    Label l3 = new Label("SUM:");  
    Button add = new Button("+"); 
    Button sub = new Button("-");
    Button mul = new Button("*");
    Button div = new Button("/");

    public void init()  
    {  
        setLayout(new GridLayout(6, 6,20,20)); 
        add(l1);  
        add(t1);  
        add(l2);  
        add(t2);  
        add(l3);  
        add(t3);  
        add(add);
        add(sub);
        add(mul);
        add(div);  
        add.addActionListener(this); 
        sub.addActionListener(this); 
        mul.addActionListener(this); 
        div.addActionListener(this);
    }  
    public void actionPerformed(ActionEvent e)  
    {  
        double n1 = Double.parseDouble(t1.getText());  
        double n2 = Double.parseDouble(t2.getText()); 

        if (e.getSource() == add)   t3.setText(" " + (n1 + n2));  
        if (e.getSource() == sub)   t3.setText(" "+(n1 - n2));
        if (e.getSource() == mul)   t3.setText(" " + (n1 * n2));  
        if (e.getSource() == div)   t3.setText(" "+(n1 / n2));
    }  
}  