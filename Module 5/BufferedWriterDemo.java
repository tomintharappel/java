import java.io.*;  
public class BufferedWriterDemo {  
public static void main(String[] args) throws Exception {     
    OutputStreamWriter writer = new  OutputStreamWriter(System.out);
    BufferedWriter buffer = new BufferedWriter(writer);  
    buffer.write("Welcome");  
    buffer.close();  
    System.out.println("Success");  
    }  
}  