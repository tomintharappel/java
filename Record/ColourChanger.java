import java.awt.*;  
import java.awt.event.*;  
import java.applet.*; 
/*<applet code="ColourChanger.java" width="300" height="300"></applet> */
public class ColourChanger extends Applet implements ActionListener  
{  
    Button red = new Button("Red");
    Button green = new Button("Green");
    Button blue = new Button("Blue");
    Button white = new Button("white");
    public void init()  
    {  
        add(red);
        add(green);
        add(blue);
        add(white);

        red.addActionListener(this); 
        green.addActionListener(this); 
        blue.addActionListener(this); 
        white.addActionListener(this);
    }  
    public void actionPerformed(ActionEvent e)  
    {   

        if (e.getSource() == red)   setBackground(Color.RED);
        if (e.getSource() == blue)   setBackground(Color.BLUE);
        if (e.getSource() == green)   setBackground(Color.GREEN);  
        if (e.getSource() == white)   setBackground(Color.WHITE);
    }  
}  