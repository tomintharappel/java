// Demonstrate TextArea. 
import java.awt.*; 
import java.applet.*; 
/* <applet code="TextAreaDemo" width=300 height=250> </applet> */ 
 
public class TextAreaDemo extends Applet {   
    public void init() {     
        String val = "Java 8 is the latest version of the most\n" + 
                    "widely-used computer language for Internet programming.\n";
 
    TextArea text = new TextArea(val, 10, 30);     
    add(text);  
 } 
}