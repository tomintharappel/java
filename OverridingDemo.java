class Base {
    public void display(String message) 
    {
        System.out.println("Method called from base class and the message is:"+ message);
    }
}
class Derived extends Base{
    public void display(String message)
    {
        System.out.println("Method called from derived class and the message is:" +message);
    }
}
class OverridingDemo {
    public static void main(String []Args) 
    {
        Base obj; //base class reference variable

        obj = new Base (); //base class object refered by base class reference variable
        obj.display("hello");

        obj = new Derived(); // derived class object refered by base class reference variable.
        obj.display("welcome");
    }
}