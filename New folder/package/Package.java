import std.Student;
import teach.Teacher;
class Package {
    public static void main(String []Arg) {

        Student s = new Student("Albert Don","BCA",23,10);
        s.getStudentDetails();

        Teacher t = new Teacher("Thomas","Java Programming");
        t.getTeacherDetails();
    }
}