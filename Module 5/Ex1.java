import java.awt.*;
import java.applet.*;

/**
 * <applet code="Ex1" width=300 height=300 >
 * </applet>
 */

public class Ex1 extends Applet {
    public void init()
    {
        setForeground(Color.red);
    }
    public void paint(Graphics g) {
       
        g.fillRect(60,10, 30,80);
        g.drawRoundRect(10,100, 80, 50, 10,10);
        g.fillRoundRect(20,110,60,30,5,5);
    }
}