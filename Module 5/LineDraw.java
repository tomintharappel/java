import java.awt.*;
import java.applet.*;

/**
 * <applet code="LineDraw" width=300 height=300 >
 * </applet>
 */

public class LineDraw extends Applet {
    public void init()
    {
        setForeground(Color.red);
    }
    public void paint(Graphics g) {
        g.drawLine(20,20, 180,180);
        g.drawLine(180,20, 20,180);
        g.drawRect(20,20,160,160);
    }
}