class Box 
{ 
    double width, height, depth; 
    Box(double w, double h, double d) 
    { 
        width = w; 
        height = h; 
        depth = d; 
    } 
    Box(Box bx) 
    {
        width = bx.width;
        height = bx.height;
        depth = bx.depth;
    }
    Box() 
    {  
        width = height = depth = 0; 
    } 
    public void showDetails()
    {
        System.out.println("Box Width: " + width);
        System.out.println("Box Height: "+ height);
        System.out.println("Box depth: " + depth );
    }
} 
class BoxTest {
    public static void main(String [] Args) {

        Box bx1 = new Box();
        System.out.println("Box 1");
        bx1.showDetails();

        Box bx2 = new Box(10,12,14);
        System.out.println("Box 2");
        bx2.showDetails();

        Box bx3 = new Box(bx2);
        System.out.println("Box 3");
        bx3.showDetails();

    }
}