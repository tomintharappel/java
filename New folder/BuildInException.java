import java.util.Scanner;
class Arithmetic {

    int number1,number2,result;
    public void divide() {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter two number");
            try 
            {
            number1 = input.nextInt();   
            number2 = input.nextInt();

            result = number1/number2;
            System.out.println("The Result is : "+result);
            }
            catch(Exception e)
            {
                System.out.println(e.toString());
            }
        System.out.println("Program Successfully Completed");
    }
}
class BuildInException{

    public static void main(String []Args) {

        Arithmetic obj = new Arithmetic();
    
            obj.divide();
        
    }
}