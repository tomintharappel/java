package std;
import java.io.*;

public class Student {
    String name;
    String course;
    int age;
    int rollno;
    public Student(String name, String course,int age,int rollno) {
        
        this.name = name;
        this.course = course;
        this.age = age;
        this.rollno = rollno;
    }
    public void getStudentDetails()
    {
        System.out.println("Student Name :" + name);
        System.out.println("Course       :" + course);
        System.out.println("Age :" + age);
        System.out.println("rollno :" + rollno);
    }
}