import java.awt.*;
import java.applet.*;

/**
 * <applet code="Arc" width=300 height=300></applet>
 */

 public class Arc extends Applet {

    public void paint(Graphics g) {

        g.drawArc(50,50,100,100,90,180);
    }
 }