import java.awt.*;
import java.applet.*;

/**
 * <applet code="Circle" width=300 height=300 >
 * </applet>
 */

public class Circle extends Applet {
    public void init()
    {
        setForeground(Color.red);
    }
    public void paint(Graphics g) {
       
        g.drawOval(50,150,100,100);
        g.drawOval(20,20, 50,100);
        g.fillOval(100,100,20,20);
    }
}