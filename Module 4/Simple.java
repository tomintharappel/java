import java.awt.*;
import java.awt.event.*;
import java.applet.*;
/**
 * <applet code="Simple" width=300 height=100></applet>
 */
public class Simple extends Applet
    implements MouseMotionListener {

        String msg = "";
        int mouseX = 0, mouseY = 0;  // coordinates of mouse

        public void init() {
            
            addMouseMotionListener(this); //registring listener
        }
        
        public void mouseMoved(MouseEvent me)
        {
            showStatus("Moving mouse at " + me.getX() + ", " + me.getY() );
        }
        public void mouseDragged(MouseEvent me) 
        {
            mouseX = me.getX();
            mouseY = me.getY();
            repaint();
        }
        public void paint(Graphics g) {
            g.drawString(msg,mouseX,mouseY);
        }
    }