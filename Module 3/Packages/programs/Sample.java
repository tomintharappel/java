import java.util.Scanner;
import java.util.InputMismatchException;
class Sample {

    public static void main (String []Args) {
        try 
        {
            Scanner input = new Scanner(System.in);

            System.out.print("Enter the first number: ");
            int number1 = input.nextInt();

            System.out.print("Enter the second number: ");
            int number2 = input.nextInt();

            int result = number2/number1;

            System.out.printf("The result is %d", result);
        } 
        catch(ArithmeticException ex) 
        {
            System.out.println("There is an error occured");
        }
        catch(InputMismatchException ex) 
        {
            System.out.println("Invalid inputs");
        }
        catch(Exception ex) 
        {
            System.out.println("An error is occured");
        }
    }
}