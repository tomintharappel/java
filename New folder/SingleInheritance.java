class Person {

    String name;
    String gender;
    int age;

    public void SetPersonInfo(String name,String gender,int age)   {

        this.name = name;
        this.gender = gender;
        this.age = age;
    }
    public void getPersonInfo() {

        System.out.println("Name  :" + name);
        System.out.println("Gender:" + gender);
        System.out.println("Age   :" + age);
    }
}
class Student extends Person{

    int rollno;
    String course;

    public void SetStudentInfo(int rollno,String course) {

        this.rollno = rollno;
        this.course = course;
    }
    public void getStudentInfo() {

        System.out.println("RollNo:" + rollno);
        System.out.println("Course:" + course);
    }
}
class SimpleInheritance {
    public static void main(String []Args) {

        Student student1 = new Student();

        student1.SetPersonInfo("Albert", "Male", 25);
        student1.SetStudentInfo(1, "BCA");

        student1.getPersonInfo();
        student1.getStudentInfo();
    }
}