class Base {
    int x;
    Base(int x) {
        this.x = x;
    }
    void display () {
        System.out.println("Invoked Base Class Method");
        System.out.println("Super x = " + x);
    }
}
class Derived extends Base {
    int y;
    Derived(int x, int y) {
        super(x);
        this.y = y;
    }
    void display()
    {
        System.out.println("Invoked Derived Class Method");
        System.out.println("Super x =" + x);
        System.out.println("Sub y =" + y);
    }
}
class Overriding {
    public static void main(String []Args) {
        Base b = new Base(30);
        b.display();
        Derived sub = new Derived(10,12);
        sub.display();
    }
}