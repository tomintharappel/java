package teach;

public class Teacher {
    String name;
    String subject;

    public Teacher(String name,String subject) {
        
        this.name = name;
        this.subject = subject;
    }
    public void getTeacherDetails()
    {
        System.out.println("Teacher Name:" + name);
        System.out.println("Subject :" + subject);
    }
}