class A extends Thread {
    public void run()
    {
        for(int i=0;i<5;i++) {

            System.out.println("\t From Thread A: i = "+i);
        }
    }
}
class B extends Thread {
    public void run()
    {
        for(int i=0;i<5;i++) {

            System.out.println("\t From Thread B: i = "+i);
        }
    }
}
class C extends Thread {
    public void run()
    {
        for(int i=0;i<5;i++) {

            System.out.println("\t From Thread C: i = "+i);
        }
    }
}
public class MultiThread {

public static void main(String[] args) {
    A thread1 = new A();
    B thread2 = new B();
    C thread3 = new C();
    thread1.start();
    thread2.start();
    thread3.start();
}
}