import java.util.Scanner;

interface Area {

    final static double pi = 3.14;

    void setDimensions();
    void computeArea ();
}
class Circle implements Area
{
    double radius;
    public void setDimensions()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the radius (Circle) ");
        radius = input.nextDouble();

    }
    public void computeArea () {

         double area = pi * radius * radius;
         System.out.println("Area (Circle) : " + area);
    }
}
class Square implements Area
{
    double side;
    public void setDimensions()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter length of a side (Square) ");
        side = input.nextDouble();
    }
    public void computeArea () {

         double area = side * side;
         System.out.println("Area (Square) : " + area);
    }
}
class InterfaceEg {

    public static void main(String []Args) {
        
        Area  obj;

        obj = new Rectangle();
        obj.setDimensions();
        obj.computeArea();

        obj = new Square();
        obj.setDimensions();
        obj.computeArea();
    }
}