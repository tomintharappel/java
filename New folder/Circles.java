import java.awt.*;
import java.applet.*;
 
public class Circles extends Applet
{
 public void paint(Graphics g)
 {
  g.drawString("Concentric Circles",30,30);
  int i=65,j=65;
  while(i>=30)
  {
   g.drawOval(i,i,j,j);   
   i = i - 10;
   j = j + 20;
  }
 }
}
 
/* <applet code=Circles width=200 height=200>
  </applet> */