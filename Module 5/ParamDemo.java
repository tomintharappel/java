import java.awt.*;
import java.applet.*;

/**
 * <applet code="ParamDemo" width=300 height=80> 
        <param name=myname value="welcome to java">
  </applet>
 */
public class ParamDemo extends Applet {

    public void paint(Graphics g)
    {
        String msg = getParameter("myname");
        g.drawString(msg,20,20);
    }
}
