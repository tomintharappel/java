class Addition {
    void sum(int num1,int num2) {

        int sum = num1 + num2;
        System.out.println(" [integer,integer] The sum :" + sum);
    }
    void sum(float num1, float num2) {

        float sum = num1 + num2;
        System.out.println(" [float,float] The sum :" + sum);
    }
    void sum(int num1,float num2) {

        float sum = num1 + num2;
        System.out.println(" [int,float] The sum :" + sum);
    }
    void sum(float num1,int num2) {

        float sum = num1 + num2;
        System.out.println(" [float,int] The sum :" + sum);
    }
}
class Overloading {

    public static void main(String [] Args) {

        Addition obj = new Addition();

        obj.sum(2,3);
        obj.sum(2.5f,3.5f);
        obj.sum(2.5f,3);
        obj.sum(10,4.5f);
    }
}